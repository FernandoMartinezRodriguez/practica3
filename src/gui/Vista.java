package gui;

import com.ferMartinez.base.Disco;
import com.ferMartinez.base.Encargado;
import com.ferMartinez.base.Revista;
import com.ferMartinez.base.Tienda;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import java.awt.*;

public class Vista extends JFrame{

    private JPanel panel1;
    JPanel panelClientes;
    JPanel panelProductos;
    JPanel panelTiendas;
    JPanel panelColecciones;
    JTabbedPane tabbedPane;

    //Revista
    JTextField txtNombreRevista;
    JTextField txtAutorRevista;
    JTextField txtPaginasRevista;
    JTextField txtVentasRevista;
    JTextField txtFechaRevista;
    JButton btnNuevoRevista;
    JButton btnModificarRevista;
    JButton btnBorrarRevista;
    JButton btnListarRevista;
    JList listaRevistas;

    //Discos
    JTextField txtCantanteDisco;
    JTextField txtCancionesDisco;
    JTextField txtVentasDisco;
    JTextField txtFechaDisco;
    JTextField txtNombreDisco;
    JList listarDiscos;
    JButton btnNuevoDisco;
    JButton btnModificarDisco;
    JButton btnBorrarDisco;
    JButton btnListarDiscos;

    //Tienda
    JTextField txtNombreTienda;
    JTextField txtDireccionTienda;
    JTextField txtCiudadTienda;
    JTextField txtTelefonoTienda;
    DatePicker dpFechaTienda;
    JList listaTiendas;
    JButton btnListarTienda;
    JButton btnNuevaTienda;
    JButton btnModificarTienda;
    JButton btnBorrarTienda;


    //Encargados
    JTextField txtNombreEncar;
    JTextField txtApellidoEncar;
    JTextField txtTelefonoEncar;

    JList listaEncargados;
    JButton btnNuevoEncar;
    JButton btnBorrarEncar;
    JButton btnModificarEncar;
    JButton btnListarEncar;

    JLabel lblNombre;
    JLabel lblDireccionTienda;
    JLabel lblCiudadTienda;
    JLabel lblTelefonoTienda;
    JRadioButton femeninoRadioButton;
    JComboBox cbTiendaEncar;
    JRadioButton masculinoRadiobutton;
    DatePicker fechaDisco;
    DatePicker fechaRevista;

    //MENUBAR
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    //OptionDialog
    OptionDialog optionDialog;
    //SAVECHANGESDIALOG
    JDialog adminPasswordDialog;
    JButton btnValidar;
    JPasswordField adminPassword;

    DefaultListModel<Encargado> dlmEncargados;
    DefaultListModel<Revista> dlmRevistas;
    DefaultListModel<Disco> dlmDiscos;
    DefaultListModel<Tienda> dlmTiendas;
    DefaultComboBoxModel dcbmTienda;


    public Vista() { startJFrame(); }

    private void startJFrame() {
        setTitle("Practica-UD3-Fernando Martínez");
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+75, this.getHeight()+50));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);

        setAdmin();
        setMenu();
        inicilizarListas();
        inicilizarCombobox();

    }

    private void inicilizarListas(){
        dlmRevistas = new DefaultListModel<>();
        listaRevistas.setModel(dlmRevistas);

        dlmDiscos = new DefaultListModel<>();
        listarDiscos.setModel(dlmDiscos);

        dlmTiendas = new DefaultListModel<>();
        listaTiendas.setModel(dlmTiendas);

        dlmEncargados = new DefaultListModel<>();
        listaEncargados.setModel(dlmEncargados);

    }

    private void inicilizarCombobox(){

        dcbmTienda =  new DefaultComboBoxModel<>();
        cbTiendaEncar.setModel(dcbmTienda);

    }

    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    private void setAdmin() {
        btnValidar = new JButton("Validar");
        btnValidar.setActionCommand("abrir Opciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidar};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);
    }

}
