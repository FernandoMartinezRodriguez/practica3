package gui;

import com.ferMartinez.base.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.io.*;
import java.util.ArrayList;
import java.util.Properties;

public class Modelo {

    SessionFactory sessionFactory;

    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    public Modelo() {
        getPropValues();
    }

    String getIP() { return ip; }
    String getUser() { return user; }
    String getPassword() { return password; }
    String getAdminPassword() { return adminPassword; }


    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Compra.class);
        configuracion.addAnnotatedClass(Disco.class);
        configuracion.addAnnotatedClass(Revista.class);
        configuracion.addAnnotatedClass(Tienda.class);
        configuracion.addAnnotatedClass(Encargado.class);
        configuracion.addAnnotatedClass(EncargadosCompra.class);

        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    public void altaEncargado(Encargado nuevoEncargado) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoEncargado);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Encargado> getEncargados() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Encargado");
        ArrayList<Encargado> lista = (ArrayList<Encargado>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarEncargado(Encargado encargadoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(encargadoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarEncargado(Encargado encargadoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(encargadoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //-----------------

    public void altaDisco(Disco nuevoDisco) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoDisco);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Disco> getDiscos() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Disco");
        ArrayList<Disco> lista = (ArrayList<Disco>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarDisco(Disco discoSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(discoSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarDisco(Disco discoborrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(discoborrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //---------------

    public void altaRevista(Revista nuevaRevista) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaRevista);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Revista> getRevistas() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Revistas");
        ArrayList<Revista> lista = (ArrayList<Revista>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarRevista(Revista revistaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(revistaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarRevista(Revista revistaBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(revistaBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //-----------------

    public void altaTienda(Tienda nuevaTienda) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaTienda);
        sesion.getTransaction().commit();

        sesion.close();
    }

    public ArrayList<Tienda> getTiendas() {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Tienda");
        ArrayList<Tienda> lista = (ArrayList<Tienda>)query.getResultList();
        sesion.close();
        return lista;
    }

    public void modificarTienda(Tienda tiendaSeleccion) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(tiendaSeleccion);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarTienda(Tienda tiendaBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(tiendaBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //-----------



    private void getPropValues() {
        InputStream entrada = null;
        try {
            Properties props = new Properties();
            String propFileName = "config.properties";

            entrada=new FileInputStream(propFileName);

            props.load(entrada);
            ip = props.getProperty("ip");
            user = props.getProperty("user");
            password = props.getProperty("pass");
            adminPassword = props.getProperty("admin");

        } catch (IOException e) {
            System.out.println(e);
        } finally {
            try {
                if (entrada != null) entrada.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

}
