package gui;

import com.ferMartinez.base.*;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class Controlador implements ActionListener, ListSelectionListener, FocusListener, ItemListener{

    private Vista vista;
    private Modelo modelo;

    public Controlador(Modelo modelo, Vista vista){
        this.vista = vista;
        this.modelo = modelo;

        inicializar();
    }

    private void inicializar(){
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addFocusListeners(this);
        addListSelectionListeners(this);
    }

    private void addActionListeners(ActionListener controlador) {
        vista.btnNuevoRevista.addActionListener(controlador);
        vista.btnNuevoDisco.addActionListener(controlador);
        vista.btnNuevaTienda.addActionListener(controlador);
        vista.btnNuevoEncar.addActionListener(controlador);

        vista.btnModificarRevista.addActionListener(controlador);
        vista.btnModificarDisco.addActionListener(controlador);
        vista.btnModificarTienda.addActionListener(controlador);
        vista.btnModificarEncar.addActionListener(controlador);

        vista.btnBorrarRevista.addActionListener(controlador);
        vista.btnBorrarDisco.addActionListener(controlador);
        vista.btnBorrarTienda.addActionListener(controlador);
        vista.btnBorrarEncar.addActionListener(controlador);

        vista.btnListarRevista.addActionListener(controlador);
        vista.btnListarDiscos.addActionListener(controlador);
        vista.btnListarTienda.addActionListener(controlador);
        vista.btnListarEncar.addActionListener(controlador);

        vista.optionDialog.btnGuardar.addActionListener(controlador);
        vista.btnValidar.addActionListener(controlador);
        vista.itemOpciones.addActionListener(controlador);
        vista.itemSalir.addActionListener(controlador);
    }

    private void addListSelectionListeners(ListSelectionListener controlador){
        vista.listaRevistas.addListSelectionListener(controlador);
        vista.listarDiscos.addListSelectionListener(controlador);
        vista.listaTiendas.addListSelectionListener(controlador);
        vista.listaEncargados.addListSelectionListener(controlador);
    }

    private void addItemListeners(ItemListener controlador){
        vista.cbTiendaEncar.addItemListener(controlador);
    }

    private void addFocusListeners(FocusListener controlador){
        vista.cbTiendaEncar.addFocusListener(controlador);
    }

    private void setOptions() {
        vista.optionDialog.tIP.setText(modelo.getIP());
        vista.optionDialog.tUsuario.setText(modelo.getUser());
        vista.optionDialog.tPassword.setText(modelo.getPassword());
        vista.optionDialog.tAdminPassword.setText(modelo.getAdminPassword());
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

            switch (actionCommand) {
                case "Opciones":
                    vista.adminPasswordDialog.setVisible(true);
                    break;
                case "abrir Opciones":
                    if(String.valueOf(vista.adminPassword.getPassword()).equals(modelo.getAdminPassword())) {
                        vista.adminPassword.setText("");
                        vista.adminPasswordDialog.dispose();
                        vista.optionDialog.setVisible(true);
                    } else {
                        Util.showErrorAlert("La contraseña introducida no es correcta.");
                    }
                    break;
                case "guardar Opciones":
                    modelo.setPropValues(vista.optionDialog.tIP.getText(), vista.optionDialog.tUsuario.getText(),
                            String.valueOf(vista.optionDialog.tPassword.getPassword()), String.valueOf(vista.optionDialog.tAdminPassword.getPassword()));
                    vista.optionDialog.dispose();
                    vista.dispose();
                    new Controlador(new Modelo(), new Vista());
                    break;
                case "Salir":
                    modelo.desconectar();
                    System.exit(0);
                    break;
                case "Desconectar":
                    modelo.desconectar();
                    break;
                case "NuevaTienda":
                    Tienda nuevaTienda = new Tienda();
                    nuevaTienda.setNombre(vista.txtNombreTienda.getText());
                    nuevaTienda.setDireccion(vista.txtDireccionTienda.getText());
                    nuevaTienda.setCiudad(vista.txtCiudadTienda.getText());
                    nuevaTienda.setTelefono(vista.txtTelefonoTienda.getText());
                    modelo.altaTienda(nuevaTienda);
                    break;
                case "NuevoEncar":
                    Encargado nuevoEncargado = new Encargado();
                    nuevoEncargado.setNombre(vista.txtNombreEncar.getText());
                    nuevoEncargado.setApellido(vista.txtApellidoEncar.getText());
                    if(vista.femeninoRadioButton.isSelected()){
                        nuevoEncargado.setSexo(vista.femeninoRadioButton.getText());
                    }
                    else if(vista.masculinoRadiobutton.isSelected())
                        nuevoEncargado.setSexo(vista.masculinoRadiobutton.getText());

                    nuevoEncargado.setTelefono(vista.txtTelefonoEncar.getText());
                    nuevoEncargado.setTienda((Tienda) vista.cbTiendaEncar.getSelectedItem());
                    modelo.altaEncargado(nuevoEncargado);
                    break;
                case "NuevoDisco":
                    Disco nuevoDisco = new Disco();
                    nuevoDisco.setNombre(vista.txtNombreDisco.getText());
                    nuevoDisco.setCantante(vista.txtCantanteDisco.getText());
                    nuevoDisco.setCanciones(Integer.valueOf(vista.txtCancionesDisco.getText()));
                    nuevoDisco.setVentas(Integer.valueOf(vista.txtVentasDisco.getText()));
                    nuevoDisco.setFechasalida(Date.valueOf(vista.fechaDisco.getDate()));
                    modelo.altaDisco(nuevoDisco);
                    break;
                case "NuevaRevista":
                    Revista nuevaRevista = new Revista();
                    nuevaRevista.setNombre(vista.txtNombreRevista.getText());
                    nuevaRevista.setAutor(vista.txtAutorRevista.getText());
                    nuevaRevista.setPaginas(Integer.valueOf(vista.txtPaginasRevista.getText()));
                    nuevaRevista.setVentas(Integer.valueOf(vista.txtVentasRevista.getText()));
                    nuevaRevista.setPublicacion(Date.valueOf(vista.fechaRevista.getDate()));
                    modelo.altaRevista(nuevaRevista);
                    break;

                case "BorrarRevista":
                    Revista revistaBorrada  = (Revista) vista.listaRevistas.getSelectedValue();
                    modelo.borrarRevista(revistaBorrada);
                    break;
                case "BorrarDisco":
                    Disco discoBorrado = (Disco) vista.listarDiscos.getSelectedValue();
                    modelo.borrarDisco(discoBorrado);
                    break;
                case "BorrarTienda":
                    Tienda tiendaBorrar = (Tienda) vista.listaTiendas.getSelectedValue();
                    modelo.borrarTienda(tiendaBorrar);
                    break;
                case "BorrarEncar":
                    Encargado encargadoBorrado  = (Encargado) vista.listaEncargados.getSelectedValue();
                    modelo.borrarEncargado(encargadoBorrado);
                    break;

                case "ModificarRevista":
                    Revista revistaSeleccion = (Revista) vista.listaRevistas.getSelectedValue();
                    revistaSeleccion.setNombre(vista.txtNombreRevista.getText());
                    revistaSeleccion.setAutor(vista.txtAutorRevista.getText());
                    revistaSeleccion.setPaginas(Integer.valueOf(vista.txtPaginasRevista.getText()));
                    revistaSeleccion.setVentas(Integer.valueOf(vista.txtVentasRevista.getText()));
                    revistaSeleccion.setPublicacion(Date.valueOf(vista.fechaRevista.getDate()));
                    modelo.modificarRevista(revistaSeleccion);
                    break;
                case "ModificarDisco":
                    Disco discoSeleccion = (Disco) vista.listarDiscos.getSelectedValue();
                    discoSeleccion.setNombre(vista.txtNombreDisco.getText());
                    discoSeleccion.setCantante(vista.txtCantanteDisco.getText());
                    discoSeleccion.setCanciones(Integer.valueOf(vista.txtCancionesDisco.getText()));
                    discoSeleccion.setVentas(Integer.valueOf(vista.txtVentasDisco.getText()));
                    discoSeleccion.setFechasalida(Date.valueOf(vista.fechaDisco.getDate()));
                    modelo.modificarDisco(discoSeleccion);
                    break;
                case "ModificarTienda":
                    Tienda modifTienda = (Tienda) vista.listaTiendas.getSelectedValue();
                    modifTienda.setNombre(vista.txtNombreTienda.getText());
                    modifTienda.setDireccion(vista.txtDireccionTienda.getText());
                    modifTienda.setCiudad(vista.txtCiudadTienda.getText());
                    modifTienda.setTelefono(vista.txtTelefonoTienda.getText());
                    modelo.modificarTienda(modifTienda);
                    break;
                case "ModificarEncar":
                    Encargado encargadoModif = (Encargado) vista.listaEncargados.getSelectedValue();
                    encargadoModif.setNombre(vista.txtNombreEncar.getText());
                    encargadoModif.setApellido(vista.txtApellidoEncar.getText());
                    if(vista.femeninoRadioButton.isSelected()){
                        encargadoModif.setSexo(vista.femeninoRadioButton.getText());
                    }
                    else if(vista.masculinoRadiobutton.isSelected())
                        encargadoModif.setSexo(vista.masculinoRadiobutton.getText());
                    encargadoModif.setTelefono(vista.txtTelefonoEncar.getText());
                    encargadoModif.setSexo(String.valueOf(vista.cbTiendaEncar.getSelectedItem()));
                    modelo.modificarEncargado(encargadoModif);
                    break;

                case "ListarDiscos":
                    listarDiscos(modelo.getDiscos());
                    break;
                case "ListarRevistas":
                    listarRevistas(modelo.getRevistas());
                    break;
                case "ListarEncargados":
                    listarEncargados(modelo.getEncargados());
                    break;
                case "ListarTiendas":
                    listarTienda(modelo.getTiendas());
                    break;
        }


    }

    private void listarDiscos(ArrayList<Disco> discos) {
        vista.dlmDiscos.clear();
        for (Disco disco: discos) {
            vista.dlmDiscos.addElement(disco);
        }
    }

    private void listarRevistas(ArrayList<Revista> revistas) {
        vista.dlmRevistas.clear();
        for (Revista revista: revistas) {
            vista.dlmRevistas.addElement(revista);
        }
    }

    private void listarEncargados(ArrayList<Encargado> encargados) {
        vista.dlmEncargados.clear();
        for (Encargado encargado: encargados) {
            vista.dlmEncargados.addElement(encargado);
        }
    }

    private void listarTienda(ArrayList<Tienda> tiendas) {
        vista.dlmTiendas.clear();
        for (Tienda tienda: tiendas) {
            vista.dlmTiendas.addElement(tienda);
        }
    }

    private void listarTiendasCBox(){
        List<Tienda> listaTiendas = modelo.getTiendas();
        vista.dcbmTienda.removeAllElements();
        for(Tienda tienda : listaTiendas){
            vista.dcbmTienda.addElement(tienda);
        }
    }

    @Override
    public void focusGained(FocusEvent e) {
        if(e.getSource() == vista.cbTiendaEncar) {
            //Cuando pincho en el combo, no despliego el menu
            vista.cbTiendaEncar.hidePopup();
            listarTiendasCBox();
            //Despues de cargar los datos, si despliego el menu
            vista.cbTiendaEncar.showPopup();
        }
    }

    @Override
    public void focusLost(FocusEvent e) {    }


    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listaRevistas) {
                Revista revistaSeleccion = (Revista) vista.listaRevistas.getSelectedValue();
                vista.txtNombreRevista.setText(revistaSeleccion.getNombre());
                vista.txtAutorRevista.setText(revistaSeleccion.getAutor());
                vista.txtPaginasRevista.setText(String.valueOf(revistaSeleccion.getPaginas()));
                vista.txtVentasRevista.setText(String.valueOf(revistaSeleccion.getVentas()));
                vista.txtFechaRevista.setText(String.valueOf(revistaSeleccion.getPublicacion()));
            }
            if(e.getSource() == vista.listarDiscos){
                Disco discoSeleccionado = (Disco) vista.listarDiscos.getSelectedValue();
                vista.txtNombreDisco.setText(discoSeleccionado.getNombre());
                vista.txtCantanteDisco.setText(discoSeleccionado.getCantante());
                vista.txtCancionesDisco.setText(String.valueOf(discoSeleccionado.getCanciones()));
                vista.txtVentasDisco.setText(String.valueOf(discoSeleccionado.getVentas()));
                vista.txtFechaDisco.setText(String.valueOf(discoSeleccionado.getFechasalida()));
            }

            if(e.getSource() == vista.listaTiendas){
                Tienda tiendaSeleccionada = (Tienda) vista.listaTiendas.getSelectedValue();
                vista.txtNombreTienda.setText((tiendaSeleccionada.getNombre()));
                vista.txtDireccionTienda.setText(tiendaSeleccionada.getDireccion());
                vista.txtCiudadTienda.setText(tiendaSeleccionada.getCiudad());
                vista.txtTelefonoTienda.setText(tiendaSeleccionada.getTelefono());
            }

            if(e.getSource() == vista.listaEncargados){
                Encargado encargadoSeleccionada = (Encargado) vista.listaEncargados.getSelectedValue();
                vista.txtNombreEncar.setText(encargadoSeleccionada.getNombre());
                vista.txtApellidoEncar.setText(encargadoSeleccionada.getApellido());
                if(vista.femeninoRadioButton.isSelected()){
                    vista.femeninoRadioButton.setText(encargadoSeleccionada.getSexo());
                }
                else if(vista.masculinoRadiobutton.isSelected())
                    vista.masculinoRadiobutton.setText(encargadoSeleccionada.getSexo());
                vista.txtTelefonoEncar.setText(encargadoSeleccionada.getTelefono());
                vista.cbTiendaEncar.setSelectedItem(encargadoSeleccionada.getTienda());
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
}
