package com.ferMartinez.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "discos", schema = "basehibernatediscos", catalog = "")
public class Disco {
    private int idDisco;
    private String nombre;
    private String cantante;
    private int canciones;
    private int ventas;
    private Date fechasalida;
    private List<Compra> compras;

    @Id
    @Column(name = "id_disco")
    public int getIdDisco() {
        return idDisco;
    }

    public void setIdDisco(int idDisco) {
        this.idDisco = idDisco;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "cantante")
    public String getCantante() {
        return cantante;
    }

    public void setCantante(String cantante) {
        this.cantante = cantante;
    }

    @Basic
    @Column(name = "canciones")
    public int getCanciones() {
        return canciones;
    }

    public void setCanciones(int canciones) {
        this.canciones = canciones;
    }

    @Basic
    @Column(name = "ventas")
    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }

    @Basic
    @Column(name = "fechasalida")
    public Date getFechasalida() {
        return fechasalida;
    }

    public void setFechasalida(Date fechasalida) {
        this.fechasalida = fechasalida;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disco disco = (Disco) o;
        return idDisco == disco.idDisco &&
                canciones == disco.canciones &&
                ventas == disco.ventas &&
                Objects.equals(nombre, disco.nombre) &&
                Objects.equals(cantante, disco.cantante) &&
                Objects.equals(fechasalida, disco.fechasalida);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idDisco, nombre, cantante, canciones, ventas, fechasalida);
    }

    @ManyToMany(mappedBy = "discos")
    public List<Compra> getCompras() {
        return compras;
    }

    public void setCompras(List<Compra> compras) {
        this.compras = compras;
    }
}
