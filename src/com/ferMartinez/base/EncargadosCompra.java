package com.ferMartinez.base;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "encargados_compras", schema = "basehibernatediscos", catalog = "")
public class EncargadosCompra {
    private int id;
    private int prima;
    private Encargado encargados;
    private Compra compras;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "prima")
    public int getPrima() {
        return prima;
    }

    public void setPrima(int prima) {
        this.prima = prima;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EncargadosCompra that = (EncargadosCompra) o;
        return id == that.id &&
                prima == that.prima;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, prima);
    }

    @ManyToOne
    @JoinColumn(name = "id_encargado", referencedColumnName = "id_encargado", nullable = false)
    public Encargado getEncargados() {
        return encargados;
    }

    public void setEncargados(Encargado encargados) {
        this.encargados = encargados;
    }

    @ManyToOne
    @JoinColumn(name = "id_compra", referencedColumnName = "id_compra", nullable = false)
    public Compra getCompras() {
        return compras;
    }

    public void setCompras(Compra compras) {
        this.compras = compras;
    }
}
