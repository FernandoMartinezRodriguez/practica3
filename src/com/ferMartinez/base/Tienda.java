package com.ferMartinez.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Tienda {
    private int idTienda;
    private String nombre;
    private String direccion;
    private String ciudad;
    private String telefono;
    private List<Encargado> encargados;

    @Id
    @Column(name = "id_tienda")
    public int getIdTienda() {
        return idTienda;
    }

    public void setIdTienda(int idTienda) {
        this.idTienda = idTienda;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "ciudad")
    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return idTienda == tienda.idTienda &&
                Objects.equals(nombre, tienda.nombre) &&
                Objects.equals(direccion, tienda.direccion) &&
                Objects.equals(ciudad, tienda.ciudad) &&
                Objects.equals(telefono, tienda.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTienda, nombre, direccion, ciudad, telefono);
    }

    @OneToMany(mappedBy = "tienda")
    public List<Encargado> getEncargados() {
        return encargados;
    }

    public void setEncargados(List<Encargado> encargados) {
        this.encargados = encargados;
    }
}
