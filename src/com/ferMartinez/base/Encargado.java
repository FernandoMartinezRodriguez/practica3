package com.ferMartinez.base;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "encargados", schema = "basehibernatediscos", catalog = "")
public class Encargado {
    private int idEncargado;
    private String nombre;
    private String apellido;
    private String sexo;
    private String telefono;
    private List<EncargadosCompra> realizan;
    private Tienda tienda;

    @Id
    @Column(name = "id_encargado")
    public int getIdEncargado() {
        return idEncargado;
    }

    public void setIdEncargado(int idEncargado) {
        this.idEncargado = idEncargado;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellido")
    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    @Basic
    @Column(name = "sexo")
    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Encargado encargado = (Encargado) o;
        return idEncargado == encargado.idEncargado &&
                Objects.equals(nombre, encargado.nombre) &&
                Objects.equals(apellido, encargado.apellido) &&
                Objects.equals(sexo, encargado.sexo) &&
                Objects.equals(telefono, encargado.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idEncargado, nombre, apellido, sexo, telefono);
    }

    @OneToMany(mappedBy = "encargados")
    public List<EncargadosCompra> getRealizan() {
        return realizan;
    }

    public void setRealizan(List<EncargadosCompra> realizan) {
        this.realizan = realizan;
    }

    @ManyToOne
    @JoinColumn(name = "id_encargado", referencedColumnName = "id_tienda", nullable = false)
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }
}
