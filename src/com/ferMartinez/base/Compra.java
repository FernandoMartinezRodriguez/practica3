package com.ferMartinez.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "compras", schema = "basehibernatediscos", catalog = "")
public class Compra {
    private int idCompra;
    private String comprador;
    private int cantidad;
    private Date fecha;
    private List<EncargadosCompra> reaalizan;
    private List<Disco> discos;
    private List<Revista> revistas;

    @Id
    @Column(name = "id_compra")
    public int getIdCompra() {
        return idCompra;
    }

    public void setIdCompra(int idCompra) {
        this.idCompra = idCompra;
    }

    @Basic
    @Column(name = "comprador")
    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    @Basic
    @Column(name = "cantidad")
    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Compra compra = (Compra) o;
        return idCompra == compra.idCompra &&
                cantidad == compra.cantidad &&
                Objects.equals(comprador, compra.comprador) &&
                Objects.equals(fecha, compra.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCompra, comprador, cantidad, fecha);
    }

    @OneToMany(mappedBy = "compras")
    public List<EncargadosCompra> getReaalizan() {
        return reaalizan;
    }

    public void setReaalizan(List<EncargadosCompra> reaalizan) {
        this.reaalizan = reaalizan;
    }

    @ManyToMany
    @JoinTable(name = "compras_discos", catalog = "", schema = "basehibernatediscos", joinColumns = @JoinColumn(name = "id_compra", referencedColumnName = "id_compra", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_disco", referencedColumnName = "id_disco", nullable = false))
    public List<Disco> getDiscos() {
        return discos;
    }

    public void setDiscos(List<Disco> discos) {
        this.discos = discos;
    }

    @ManyToMany
    @JoinTable(name = "compras_revistas", catalog = "", schema = "basehibernatediscos", joinColumns = @JoinColumn(name = "id_compra", referencedColumnName = "id_compra", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_revista", referencedColumnName = "id_revista", nullable = false))
    public List<Revista> getRevistas() {
        return revistas;
    }

    public void setRevistas(List<Revista> revistas) {
        this.revistas = revistas;
    }
}
