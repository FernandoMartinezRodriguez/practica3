package com.ferMartinez.base;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "revistas", schema = "basehibernatediscos", catalog = "")
public class Revista {
    private int idRevista;
    private String nombre;
    private String autor;
    private int paginas;
    private int ventas;
    private Date publicacion;
    private List<Compra> compras;

    @Id
    @Column(name = "id_revista")
    public int getIdRevista() {
        return idRevista;
    }

    public void setIdRevista(int idRevista) {
        this.idRevista = idRevista;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "autor")
    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    @Basic
    @Column(name = "paginas")
    public int getPaginas() {
        return paginas;
    }

    public void setPaginas(int paginas) {
        this.paginas = paginas;
    }

    @Basic
    @Column(name = "ventas")
    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }

    @Basic
    @Column(name = "publicacion")
    public Date getPublicacion() {
        return publicacion;
    }

    public void setPublicacion(Date publicacion) {
        this.publicacion = publicacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Revista revista = (Revista) o;
        return idRevista == revista.idRevista &&
                paginas == revista.paginas &&
                ventas == revista.ventas &&
                Objects.equals(nombre, revista.nombre) &&
                Objects.equals(autor, revista.autor) &&
                Objects.equals(publicacion, revista.publicacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRevista, nombre, autor, paginas, ventas, publicacion);
    }

    @ManyToMany(mappedBy = "revistas")
    public List<Compra> getCompras() {
        return compras;
    }

    public void setCompras(List<Compra> compras) {
        this.compras = compras;
    }
}
